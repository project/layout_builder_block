Description
-----------

The module provides a block plugin that allows to place a simple formatted text
blob as a Layout Builder component. The text is saved as component configuration
which is stored into the layout storage, meaning that, unlike Block Content,
there's no external object. Everything is Layout Builder self-contained. Other
advantage deriving from this architecture is that such blocks are contained in
configuration, thus they can be imported/exported.

Dependencies
------------

* Layout Builder (layout_builder)
* Filter (filter)

Usage
-----

When clicking "Add block" in a Layout Builder section, a new _Create text block_
link will appear. Following the link, the site builder is able to enter a title
and a text, along with its text format. The content of the block can be also
edited from the Layout Builder UI, by clicking the _Configure_ contextual menu
link.

Limitations
-----------

Blocks created by this module are just simple formatted text blobs, thus they
cannot be configured in the same way as _Block Content_ (`block_content`)
blocks. It's not possible to create block types, add fields, use different
widgets, configure form and view displays. Also, the block visibility follows
the layout access permissions meaning that it's not possible to configure
different access rules.

Author
------

Claudiu Cristea: https://www.drupal.org/u/claudiucristea
