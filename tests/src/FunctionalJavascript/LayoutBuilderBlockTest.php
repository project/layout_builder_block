<?php

namespace Drupal\Tests\layout_builder_block\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\node\Entity\NodeType;

/**
 * Tests Layout Builder Block module.
 *
 * @group layout_builder_block
 */
class LayoutBuilderBlockTest extends WebDriverTestBase {

  use ContextualLinkClickTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'contextual',
    'field_ui',
    'node',
    'layout_builder_block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * Tests Layout Builder Block module.
   */
  public function testLayoutBuilderBlock(): void {
    NodeType::create(['type' => 'page'])->save();
    $this->drupalLogin($this->createUser([
      'access contextual links',
      'administer content types',
      'administer node display',
      'administer node fields',
      'configure any layout',
    ]));
    $this->drupalGet('/admin/structure/types/manage/page/display');

    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    $this->submitForm(['layout[enabled]' => TRUE], 'Save');
    $this->clickLink('Manage layout');
    $this->clickLink('Add block');
    $assert->assertWaitOnAjaxRequest();
    $assert->linkExists('Create text block');

    // Create a text block.
    $page->clickLink('Create text block');
    $assert->assertWaitOnAjaxRequest();

    $page->findField('Title')->setValue('Very basic text block');
    $page->findField('Content')->setValue('Lorem ipsum dolor sit amet');
    $page->pressButton('Add block');
    $assert->assertWaitOnAjaxRequest();
    $this->drupalGet('/admin/structure/types/manage/page/display/default/layout');
    $assert->pageTextContains('Very basic text block');
    $assert->pageTextContains('Lorem ipsum dolor sit amet');

    // Edit the simple block using contextual links.
    $this->clickContextualLink('#layout-builder .block-layout-builder-block', 'Configure', TRUE);
    $assert->assertWaitOnAjaxRequest();
    $page->findField('Content')->setValue('You know... the brown fox');
    $page->pressButton('Update');

    $assert->assertWaitOnAjaxRequest();
    $this->drupalGet('/admin/structure/types/manage/page/display/default/layout');
    $assert->pageTextContains('You know... the brown fox');

    // Save the layout and test a node.
    $page->pressButton('Save layout');
    $page->pressButton('Save');
    $this->drupalGet('/node/add/page');
    $this->submitForm(['title[0][value]' => 'Whatever'], 'Save');
    $assert->pageTextContains('Whatever has been created.');
    $assert->pageTextContains('Very basic text block');
    $assert->pageTextContains('You know... the brown fox');
    $node_url = $this->getSession()->getCurrentUrl();

    // Remove block from layout.
    $this->drupalGet('/admin/structure/types/manage/page/display/default/layout');
    $this->clickContextualLink('#layout-builder .block-layout-builder-block', 'Remove block', TRUE);
    $assert->assertWaitOnAjaxRequest();
    $assert->pageTextContains('Are you sure you want to remove the Very basic text block block?');
    $assert->pageTextContains('This action cannot be undone.');
    $page->pressButton('Remove');
    $assert->assertWaitOnAjaxRequest();
    $this->drupalGet('/admin/structure/types/manage/page/display/default/layout');
    $assert->pageTextNotContains('Very basic text block');
    $assert->pageTextNotContains('You know... the brown fox');
    $page->pressButton('Save layout');
    $page->pressButton('Save');
    $this->drupalGet($node_url);
    $assert->pageTextNotContains('You know... the brown fox');
  }

}
