<?php

namespace Drupal\layout_builder_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Block used to add a simple formatted text into a Layout Builder component.
 *
 * @Block(
 *   id = "layout_builder_block",
 *   admin_label = @Translation("Layout Builder Block"),
 * )
 */
class LayoutBuilderBlockBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'text' => '',
      'format' => filter_default_format(),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Override the parent to avoid showing the admin_label on new blocks.
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $content = $form_state->getValue('content');
    $this->setConfigurationValue('text', $content['value']);
    $this->setConfigurationValue('format', $content['format']);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Content'),
      '#format' => $this->getConfiguration()['format'],
      '#default_value' => $this->getConfiguration()['text'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#type' => 'processed_text',
      '#text' => $this->getConfiguration()['text'],
      '#format' => $this->getConfiguration()['format'],
    ];
  }

}
